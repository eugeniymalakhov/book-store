package dto

import "github.com/go-playground/validator/v10"

type SignupDTO struct {
	FirstName string `json:"firstName" validate:"required,min=3,max=32"`
	LastName  string `json:"lastName" validate:"required,min=3,max=32"`
	Email     string `json:"email" validate:"required,email"`
	Password  string `json:"password" validate:"required"`
}

type LoginDTO struct {
	Email    string `json:"email" validate:"required,email"`
	Password string `json:"password" validate:"required"`
}

// Error Responses
type ErrorResponse struct {
	FailedField string
	Tag         string
	Value       string
}

// validators
var validate = validator.New()

func Validate[T SignupDTO | LoginDTO](data *T) []*ErrorResponse {
	var errors []*ErrorResponse
	err := validate.Struct(data)
	if err != nil {
		for _, err := range err.(validator.ValidationErrors) {
			var element ErrorResponse
			element.FailedField = err.StructNamespace()
			element.Tag = err.Tag()
			element.Value = err.Param()
			errors = append(errors, &element)
		}
	}
	return errors
}
