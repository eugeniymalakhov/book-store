package dto

import "mime/multipart"

type CreateBook struct {
	Title       string `json:"title" validate:"required,min=3,max=32"`
	Description string `json:"description" validate:"required,min=10,max=256"`
	File        *multipart.FileHeader
}
