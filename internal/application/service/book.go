package service

import (
	"book-store/internal/application/dto"
	"book-store/internal/domain/model"
	"book-store/internal/domain/repository"
	"errors"
	"fmt"
	"github.com/gofiber/fiber/v2"
	"github.com/google/uuid"
	"log"
	"strings"
)

type BookService struct {
	bookRepository *repository.BookRepository
}

func NewBookService(bookRepository *repository.BookRepository) *BookService {
	return &BookService{
		bookRepository: bookRepository,
	}
}

func (service BookService) CreateBook(data *dto.CreateBook, ctx *fiber.Ctx) error {
	// => "example.pdf" 360641 "application/pdf"
	fmt.Println(data.File.Filename, data.File.Size, data.File.Header["Content-Type"][0])

	format := strings.Split(data.File.Filename, ".")[1]
	fileName := uuid.New().String() + "." + format

	// Save the files to disk:
	if err := ctx.SaveFile(data.File, fmt.Sprintf("./files/%s", fileName)); err != nil {
		log.Println("error while storing file", err.Error())
		return errors.New("error while storing file")
	}

	var book model.Book
	book.Title = data.Title
	book.Description = data.Description
	book.User = ctx.Locals("user").(model.User)
	book.Link = "./files/" + fileName

	err := service.bookRepository.CreateBook(&book)

	if err != nil {
		return err
	}
	return nil
}

func (service BookService) GetPersonalBooks(userId *int32) (books *[]model.Book, err error) {
	books, err = service.bookRepository.GetBooksByUserId(userId)
	return
}
