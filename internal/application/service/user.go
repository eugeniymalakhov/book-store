package service

import (
	"book-store/internal/application/dto"
	"book-store/internal/domain/model"
	"book-store/internal/domain/repository"
	"errors"
	"github.com/golang-jwt/jwt/v5"
	"github.com/spf13/viper"
	"golang.org/x/crypto/bcrypt"
	"time"
)

type UserService struct {
	userRepository *repository.UserRepository
}

func NewUserService(userRepository *repository.UserRepository) *UserService {
	return &UserService{
		userRepository: userRepository,
	}
}

func (service UserService) SignUp(data *dto.SignupDTO) (*model.User, error) {
	hash, err := bcrypt.GenerateFromPassword([]byte(data.Password), 10)

	if err != nil {
		return nil, err
	}

	var user model.User
	user.FirstName = data.FirstName
	user.LastName = data.LastName
	user.Email = data.Email
	user.Password = string(hash)

	err = service.userRepository.CreateUser(&user)
	if err != nil {
		return nil, err
	}

	return &user, nil
}

func (service UserService) Login(data *dto.LoginDTO) (*string, error) {
	user, err := service.userRepository.GetByEmail(&data.Email)

	if err != nil {
		return nil, err
	}

	err = bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(data.Password))

	if err != nil {
		return nil, errors.New("incorrect password")
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"sub": user.Id,
		"exp": time.Now().Add(time.Hour * 24 * 30).Unix(),
	})

	tokenString, err2 := token.SignedString([]byte(viper.Get("JWT_SECRET").(string)))

	if err2 != nil {
		return nil, err2
	}

	return &tokenString, nil
}

func (service UserService) FindById(Id *int32) (*model.User, error) {
	user, err := service.userRepository.GetUserById(Id)

	if err != nil {
		return nil, err
	}

	return user, nil
}
