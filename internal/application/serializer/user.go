package serializer

type Login struct {
	Token string `json:"token"`
}

type SignUp struct {
	Id        int32  `json:"id"`
	FirstName string `json:"firstName"`
	LastName  string `json:"lastName"`
	Email     string `json:"email"`
}
