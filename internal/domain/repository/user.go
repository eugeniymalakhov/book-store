package repository

import (
	"book-store/internal/domain/model"
	"errors"
	"gorm.io/gorm"
)

type UserRepository struct {
	db *gorm.DB
}

func NewUserRepository(db *gorm.DB) *UserRepository {
	return &UserRepository{
		db: db,
	}
}

func (r UserRepository) GetByEmail(email *string) (user *model.User, err error) {
	if res := r.db.First(&user, "email = ?", email); res.Error != nil {
		err = errors.New("user doesn't exist")
		return
	}
	return
}

func (r UserRepository) CreateUser(user *model.User) error {
	if res := r.db.Create(&user); res.Error != nil {
		return res.Error
	}
	return nil
}

func (r UserRepository) GetUserById(Id *int32) (user *model.User, err error) {
	if res := r.db.First(&user, Id); res.Error != nil {
		err = res.Error
		return
	}
	return
}
