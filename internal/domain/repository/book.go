package repository

import (
	"book-store/internal/domain/model"
	"gorm.io/gorm"
	"log"
	"os"
)

type BookRepository struct {
	db *gorm.DB
}

func NewBookRepository(db *gorm.DB) *BookRepository {
	return &BookRepository{
		db: db,
	}
}

func (r BookRepository) CreateBook(book *model.Book) error {
	if res := r.db.Create(&book); res != nil {
		log.Println("error while storing book to db")
		e := os.Remove(book.Link)
		if e != nil {
			log.Println("error while deleting book file")
		}
		return res.Error
	}
	return nil
}

func (r BookRepository) GetBooksByUserId(userId *int32) (books *[]model.Book, err error) {
	if res := r.db.Find(&books).Where("user_id = ?", userId); res.Error != nil {
		log.Println("error while fetching personal books from db")
		err = res.Error
		return
	}
	return
}
