package model

import "time"

type Book struct {
	Id          int32     `json:"id" gorm:"primaryKey"`
	UserId      int32     `json:"userId"`
	User        User      `gorm:"foreignKey:UserId"`
	Title       string    `json:"title"`
	Description string    `json:"description"`
	Link        string    `json:"link"`
	CreatedAt   time.Time `json:"createdAt" gorm:"autoCreateTime"`
	UpdatedAt   time.Time `json:"updatedAt" gorm:"autoUpdateTime"`
}
