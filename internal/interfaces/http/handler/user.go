package handler

import (
	"book-store/internal/application/dto"
	"book-store/internal/application/serializer"
	"book-store/internal/application/service"
	"github.com/gofiber/fiber/v2"
	"net/http"
)

type UserHandler struct {
	userService *service.UserService
}

func NewUserHandler(userService *service.UserService) *UserHandler {
	return &UserHandler{
		userService: userService,
	}
}

// SignUp godoc
//
//	@Summary		Sign-up.
//	@Description	sign-up process
//	@Tags			user
//	@Accept			json
//	@Param			json	body	dto.SignupDTO	false	"request data"
//	@Produce		json
//	@Success		201	{object}	serializer.SignUp
//	@Router			/api/v1/user/sign-up [post]
func (h *UserHandler) SignUp(ctx *fiber.Ctx) error {
	var data dto.SignupDTO
	if err := ctx.BodyParser(&data); err != nil {
		return err
	}

	errors := dto.Validate(&data)
	if errors != nil {
		return ctx.Status(fiber.StatusBadRequest).JSON(errors)
	}

	user, err := h.userService.SignUp(&data)
	if err != nil {
		return ctx.Status(http.StatusInternalServerError).JSON(fiber.Map{
			"error": err.Error(),
		})
	}

	return ctx.Status(http.StatusCreated).JSON(&serializer.SignUp{
		FirstName: user.FirstName,
		LastName:  user.LastName,
		Email:     user.Email,
	})
}

// Login godoc
//
//	@Summary		Login.
//	@Description	login process
//	@Tags			user
//	@Accept			json
//	@Param			json	body	dto.LoginDTO	false	"request data"
//	@Produce		json
//	@Success		200	{object}	serializer.Login
//	@Router			/api/v1/user/login [post]
func (h *UserHandler) Login(ctx *fiber.Ctx) error {
	var data dto.LoginDTO
	if err := ctx.BodyParser(&data); err != nil {
		return err
	}

	errors := dto.Validate(&data)
	if errors != nil {
		return ctx.Status(fiber.StatusBadRequest).JSON(errors)
	}

	tokenString, err := h.userService.Login(&data)
	if err != nil {
		return ctx.Status(http.StatusInternalServerError).JSON(fiber.Map{
			"error": err.Error(),
		})
	}

	return ctx.Status(http.StatusCreated).JSON(&serializer.Login{
		Token: *tokenString,
	})
}
