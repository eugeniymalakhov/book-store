package handler

import (
	"book-store/internal/application/serializer"
	"github.com/gofiber/fiber/v2"
	"net/http"
)

type HealthHandler struct{}

func NewHealthCheckHandler() *HealthHandler {
	return &HealthHandler{}
}

// HealthCheck godoc
//
//	@Summary		Show the status of server.
//	@Description	get the status of server.
//	@Tags			health
//	@Accept			*/*
//	@Produce		json
//	@Success		200	{object}	serializer.HealthCheck
//	@Router			/health [get]
func (h *HealthHandler) HealthCheck(ctx *fiber.Ctx) error {
	return ctx.Status(http.StatusOK).JSON(&serializer.HealthCheck{
		Message: "Server works",
	})
}
