package handler

import (
	"book-store/internal/application/dto"
	"book-store/internal/application/serializer"
	"book-store/internal/application/service"
	"book-store/internal/domain/model"
	"errors"
	"github.com/gofiber/fiber/v2"
	"net/http"
)

type BookHandler struct {
	bookService *service.BookService
}

func NewBookHandler(bookService *service.BookService) *BookHandler {
	return &BookHandler{
		bookService: bookService,
	}
}

// CreateBook godoc
//
//	@Summary		Create new book.
//	@Description	New book creation process
//	@Tags			book
//	@Accept			multipart/form-data
//	@Produce		json
//	@Security		ApiKeyAuth
//	@Success		201	{object}	string
//	@Router			/api/v1/book [post]
func (h *BookHandler) CreateBook(ctx *fiber.Ctx) error {
	if form, err := ctx.MultipartForm(); err == nil {
		// Get all files from "documents" key:
		files := form.File["book"]

		if len(files) > 1 {
			return ctx.Status(http.StatusBadRequest).JSON(fiber.Map{
				"error": "too many files uploaded, Should be only one",
			})
		}

		var book dto.CreateBook
		if err := ctx.BodyParser(&book); err != nil {
			return err
		}

		for _, file := range files {
			book.File = file
		}

		err := h.bookService.CreateBook(&book, ctx)
		if err != nil {
			return ctx.Status(http.StatusInternalServerError).JSON(fiber.Map{
				"error": err.Error(),
			})
		}

	}
	return ctx.Status(http.StatusBadRequest).JSON(fiber.Map{
		"error": errors.New("multipart form error"),
	})
}

// GetPersonalBooks godoc
//
//	@Summary		Get all personal books.
//	@Description	the process which return all personal books by user from token
//	@Tags			book
//	@Accept			json
//	@Produce		json
//	@Security		ApiKeyAuth
//	@Success		200	{array}	serializer.Book
//	@Router			/api/v1/book/personal [get]
func (h *BookHandler) GetPersonalBooks(ctx *fiber.Ctx) error {
	user := ctx.Locals("user").(*model.User)

	books, err := h.bookService.GetPersonalBooks(&user.Id)

	if err != nil {
		return ctx.Status(http.StatusInternalServerError).JSON(fiber.Map{
			"error": err.Error(),
		})
	}

	var response []serializer.Book

	for _, book := range *books {
		b := &serializer.Book{
			Id:          book.Id,
			Title:       book.Title,
			Description: book.Description,
			Link:        book.Link,
			CreatedAt:   book.CreatedAt,
			UpdatedAt:   book.UpdatedAt,
		}
		response = append(response, *b)
	}

	return ctx.Status(http.StatusOK).JSON(response)
}
