package internal

import (
	config2 "book-store/config"
	_ "book-store/docs"
	"book-store/internal/application/service"
	"book-store/internal/domain/repository"
	"book-store/internal/interfaces/http/handler"
	"book-store/pkg/database"
	"book-store/pkg/middleware"
	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/recover"
	fiberSwagger "github.com/swaggo/fiber-swagger"
	"log"
)

// @title						Book Store Example API
// @version					1.0
// @description				This is a sample swagger for Book store
// @host						localhost:3000
// @securityDefinitions.apikey	ApiKeyAuth
// @in							header
// @name						Authorization
// @BasePath					/
func Run() {
	config, _ := config2.LoadConfig()

	app := fiber.New()
	app.Use(recover.New())
	app.Get("/swagger/*", fiberSwagger.WrapHandler)

	// Init database connection
	db := database.Init(config.DatabaseUrl)

	// Instantiate necessary dependencies
	userRepository := repository.NewUserRepository(db)
	userService := service.NewUserService(userRepository)

	bookRepository := repository.NewBookRepository(db)
	bookService := service.NewBookService(bookRepository)

	// Create an instance of the Handlers
	healthHandler := handler.NewHealthCheckHandler()
	userHandler := handler.NewUserHandler(userService)
	bookHandler := handler.NewBookHandler(bookService)

	//Instantiate necessary middlewares
	authMiddleware := middleware.NewAuthMiddleware(userService)

	app.Get("/health", healthHandler.HealthCheck)

	// user routing
	v1 := app.Group("/api/v1")
	userRoute := v1.Group("/user")
	{
		userRoute.Post("/login", userHandler.Login)
		userRoute.Post("/sign-up", userHandler.SignUp)
	}

	// book routing
	bookRoute := v1.Group("/book")
	{
		bookRoute.Post("/",
			authMiddleware.Authenticate,
			bookHandler.CreateBook,
		)
		bookRoute.Get("/personal",
			authMiddleware.Authenticate,
			bookHandler.GetPersonalBooks,
		)
	}

	log.Fatal(app.Listen(config.Port))
}
