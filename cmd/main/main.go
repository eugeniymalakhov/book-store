package main

import (
	"book-store/internal"
)

func main() {
	internal.Run()
}
