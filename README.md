to pull repo from docker:
- docker pull winsagro/book-store:latest

run container locally:
- docker run -d -p 3000:3000 --name book-store --network dockertools_default -e DATABASE_URL=postgres://postgres:postgres@dockertools_postgres_1:5432/test -e JWT_SECRET=0dbc62332260%6b3$^%bu3$%5b6mK8treXE -e PORT=:3000 winsagro/book-store
