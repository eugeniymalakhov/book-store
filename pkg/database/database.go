package database

import (
	"book-store/internal/domain/model"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
	"log"
	"os"
)

func Init(url string) *gorm.DB {
	db, err := gorm.Open(postgres.Open(url), &gorm.Config{})

	if err != nil {
		log.Fatal("Failed to connect to database! \n", err.Error())
		os.Exit(2)
	}

	log.Println("Connected to database successfully")
	db.Logger = logger.Default.LogMode(logger.Info)

	// Auto migrate add
	dbErr := db.AutoMigrate(&model.User{}, &model.Book{})
	if dbErr != nil {
		log.Println("Error which migrations \n", dbErr.Error())
		os.Exit(2)
	}

	return db
}
