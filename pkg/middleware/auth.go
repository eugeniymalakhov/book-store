package middleware

import (
	"book-store/internal/application/service"
	"github.com/gofiber/fiber/v2"
	"github.com/golang-jwt/jwt/v5"
	"github.com/spf13/viper"
	"net/http"
	"time"
)

type AuthMiddleware struct {
	userService *service.UserService
}

func NewAuthMiddleware(userService *service.UserService) *AuthMiddleware {
	return &AuthMiddleware{
		userService: userService,
	}
}

func (m *AuthMiddleware) Authenticate(ctx *fiber.Ctx) error {
	tokenString := ctx.Get("Authorization")
	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		return []byte(viper.Get("JWT_SECRET").(string)), nil
	})

	if err != nil {
		return ctx.Status(http.StatusUnauthorized).JSON(fiber.Map{
			"error": "Incorrect token",
		})
	}

	if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
		if float64(time.Now().Unix()) > claims["exp"].(float64) {
			return ctx.Status(http.StatusUnauthorized).JSON(fiber.Map{
				"error": "Token has expired",
			})
		}

		userId := int32(claims["sub"].(float64))

		user, err := m.userService.FindById(&userId)

		if err != nil {
			return ctx.Status(http.StatusUnauthorized).JSON(fiber.Map{
				"error": "User is incorrect",
			})
		}

		ctx.Locals("user", user)
	}

	return ctx.Next()
}
