FROM golang:1.20.5

ADD . /app
WORKDIR /app

# Download Go modules
COPY go.mod go.sum ./
RUN go mod download

# Build
RUN CGO_ENABLED=0 GOOS=linux go build -tags pro -o /book-store book-store/cmd/main

EXPOSE 3000

# Run
CMD ["/book-store"]